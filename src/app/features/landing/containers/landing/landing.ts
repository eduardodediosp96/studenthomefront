import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';

// import { Movie } from '../../models/movie';
// import { fromMoviesActions } from '../../store/movies.action';
// import { getAllMovies } from '../../store/movies.selector';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'gdp-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit, OnDestroy {
breakpoint:any ="";
countries:any =[
  {name:"Estanbul",image:"assets/img/country_2.jpg"},
  {name:"Nuevo México",image:"assets/img/country_3.jpg"},
  {name:"Roma",image:"assets/img/country_10.jpg"},
  {name:"Páris",image:"assets/img/country_6.jpg"},
  {name:"Rio de Janeiro",image:"assets/img/country_4.jpg"},
  {name:"Hong Kong",image:"assets/img/country_7.jpg"},
  {name:"Tokio",image:"assets/img/country_8.jpg"},
  {name:"Moscú",image:"assets/img/country_9.jpg"},
  {name:"Sevilla",image:"assets/img/country_10.jpg"},
  {name:"Sidney",image:"assets/img/country_11.jpg"},
  {name:"Lima",image:"assets/img/country_12.jpg"},
  {name:"Cuzco",image:"assets/img/country_13.jpg"},
  {name:"Caracas",image:"assets/img/country_14.jpg"},
  {name:"Bogota",image:"assets/img/country_15.jpg"},
  {name:"New York",image:"assets/img/country_6.jpg"}
]
onResize(event) {
  this.breakpoint = (event.target.innerWidth >= 700) ? 5 : 2;
}
  constructor(
    private store: Store<any>,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.breakpoint = (window.innerWidth >= 700) ? 5 : 2;
  }
  ngOnDestroy(): void {
  }

  resize(){
    this.breakpoint = (window.innerWidth >= 400) ? 2 : 5;
  }

}
